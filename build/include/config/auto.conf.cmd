deps_config := \
	C:\git-sdk-64\esp\esp-idf\components\app_trace\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\aws_iot\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\bt\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\driver\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\esp32\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\esp_adc_cal\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\esp_event\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\esp_http_client\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\esp_http_server\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\ethernet\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\fatfs\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\freemodbus\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\freertos\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\heap\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\libsodium\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\log\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\lwip\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\mbedtls\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\mdns\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\mqtt\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\nvs_flash\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\openssl\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\pthread\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\spi_flash\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\spiffs\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\tcpip_adapter\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\unity\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\vfs\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\wear_levelling\Kconfig \
	C:\git-sdk-64\esp\esp-idf\components\app_update\Kconfig.projbuild \
	C:\git-sdk-64\esp\esp-idf\components\bootloader\Kconfig.projbuild \
	C:\git-sdk-64\esp\esp-idf\components\esptool_py\Kconfig.projbuild \
	C:\git-sdk-64\esp\esp-idf\components\partition_table\Kconfig.projbuild \
	/esp/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(IDF_TARGET)" "esp32"
include/config/auto.conf: FORCE
endif
ifneq "$(IDF_CMAKE)" "n"
include/config/auto.conf: FORCE
endif

$(deps_config): ;
