// FreeRTOS includes
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


// I2C driver
#include "driver/i2c.h"

// Error library
#include "esp_err.h"

#define ACK_CHECK_EN                       0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                      0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                            0x0              /*!< I2C ack value */
#define NACK_VAL                           0x1              /*!< I2C nack value */

//Global Variables
//uint8_t Bytes[6];

// loop task
void loop_task(void *pvParameter)
{
    while(1) { 
		vTaskDelay(1000 / portTICK_RATE_MS);		
    }
}

uint8_t* read_sensor(int sensor_address, uint8_t Bytes[]) 
{
	printf("Sending command to address 0x%02x\r\n", sensor_address);
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	
	// Setup i2c packet to initiate sensor read
	// 7CA2 sends temp then humidity
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (sensor_address << 1) | I2C_MASTER_WRITE, true);
    uint8_t cmd_meas[2] = { 0x7C, 0xA2 };
    i2c_master_write(cmd, cmd_meas, 2, true);
    i2c_master_stop(cmd);
	
	// Send i2c packet
    if(i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS) == ESP_OK) {
		printf("-> Command sent to device with address 0x%02x\r\n", sensor_address);
	}
    i2c_cmd_link_delete(cmd); // delete i2c packet

    vTaskDelay(15 / portTICK_RATE_MS); //15ms delay

	// Setup i2c packet to recieve sensor data
    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (sensor_address << 1) | I2C_MASTER_READ, true);
    i2c_master_read(cmd, Bytes, 6, false);
    i2c_master_stop(cmd); 

	// Send i2c packet
    if(i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS) == ESP_OK) {
		printf("-> Data commands sent\r\n");
	}

    i2c_cmd_link_delete(cmd); // delete i2c packet
	
	return Bytes;
	
}

void i2c_config(int sda, int scl)
{
	// configure the i2c controller 0 in master mode, normal speed
	i2c_config_t conf;
	conf.mode = I2C_MODE_MASTER;
	conf.sda_io_num = sda;
	conf.scl_io_num = scl;
	conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
	conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
	conf.master.clk_speed = 100000;
	ESP_ERROR_CHECK(i2c_param_config(I2C_NUM_0, &conf));
	printf("- i2c controller configured\r\n");
	
	// install the driver
	ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0));
	printf("- i2c driver installed\r\n\r\n");
}

float temp_convert(uint8_t Bytes[])
{
	uint16_t temp = 256U*Bytes[0]+Bytes[1];
	
	float temp_conv_c = 0-45+175*(float)temp/65536;
	float temp_conv_f = temp_conv_c*9/5+32;
	
	return temp_conv_f;
}

float humidity_convert(uint8_t Bytes[])
{
	uint16_t humi = 256U*Bytes[3]+Bytes[4];
	
	float humi_conv = 100*(float)humi/65536;
	
	return humi_conv;
}

void app_main() 
{

    uint8_t Bytes[6];

	printf("i2c scanner\r\n\r\n");

	int sda = 23;
	int scl = 22;
    int address = 0x70;
	float temp_conv_f, humi_conv;

	i2c_config(sda, scl);
	read_sensor(address, Bytes);
	
    printf("\r\nPrinting Results\r\n");
	
	temp_conv_f = temp_convert(Bytes);
	humi_conv = humidity_convert(Bytes);
	
	printf("-> Temperature %.2f\r\n", temp_conv_f);
	printf("-> Humidity %.2f\r\n", humi_conv);

    xTaskCreate(&loop_task, "loop_task", 2048, NULL, 5, NULL);
}